// Playground - noun: a place where people can play

import Foundation

class PFObject {
    
    let parseClassName : String
    
    init(className:String) {
        
        self.parseClassName = className
    }
    class func save(object:AnyObject) -> AnyObject {
        
        return "Your object \(object) was saved successfully"
    }
}


var arrayOfObjects : [AnyObject] = ["play", "ground", 123, 456, ["dictionary":"object"], PFObject(className: "parseObject")]


var randIndex = Int(arc4random_uniform(UInt32(arrayOfObjects.count)))

println(arrayOfObjects[randIndex])

